import './App.css'

import React, { Component } from 'react'

import moment from 'moment'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      percentageTime: ''
    }
  }

  componentWillMount() {
    this.getPercentageTime()

    setInterval(() => this.getPercentageTime(), 1000)
  }

  componentDidMount() {
    const input = document.querySelector('input')

    input && input.focus()
  }

  getUrlParameter(name) {
    name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]')
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
    let results = regex.exec(window.location.search)
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
  }

  getPercentageTime() {
    const startOfDay = moment()
      .startOf('day')
      .add(9, 'hours')
      .subtract(30, 'minute')

    let endOfDay = moment()
      .startOf('day')
      .add(17, 'hours')

    // For early finish fridays
    if (moment().day() === 5) endOfDay = endOfDay.subtract(30, 'minute')

    const percentage = (100 * ((moment() - startOfDay) / (endOfDay - startOfDay)) - 1).toFixed(2)

    this.setState({
      percentageTime: percentage.toString()
    })
  }

  render() {
    return (
      <main>
        <div>
          <h1>
            Hi {this.getUrlParameter('name') || 'there'}, you're&nbsp;
            <span>{this.state.percentageTime}%</span>&nbsp;through your day.
          </h1>

          <form action="https://www.google.com/search">
            <input placeholder="Search Google" name="q" />
          </form>
        </div>
      </main>
    )
  }
}
